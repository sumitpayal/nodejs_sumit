const mongoose = require('mongoose')
const { isEmail } = require('validator')
// const Ajv = require('ajv')

// const ajv = new Ajv()

const userSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        unique: true,
        required: true,
        validate: [ isEmail, 'invalid email' ] 
    },
    mobile: {
        type: Number
    },
    role: {
        type: String,
        enum:['User','Admin'],
        default: 'User'
    },
    address: [String],
    password: {
        type: String,
        required: true
    }
}); 

module.exports = mongoose.model('User',userSchema);